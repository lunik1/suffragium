(ns suffragium.runoff-bench
  (:require [libra.bench :refer :all]
            [libra.criterium :as c]
            [suffragium.runoff :refer :all]
            [suffragium.example-ballots :refer :all]))

(defbench irv-beatpath12
  (is (c/bench (irv beatpath12 :all-rounds true))))
