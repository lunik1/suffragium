(ns suffragium.stochastic-bench
  (:require [libra.bench :refer :all]
            [libra.criterium :as c]
            [suffragium.stochastic :refer :all]
            [suffragium.example-ballots :refer :all]))

(defbench ssv-beatpath12
  (is (c/bench (ssv beatpath12))))
