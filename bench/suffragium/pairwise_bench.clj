(ns suffragium.pairwise-bench
  (:require [libra.bench :refer :all]
            [libra.criterium :as c]
            [suffragium.pairwise :refer :all]
            [suffragium.example-ballots :refer :all]))

(defbench pairwise-beatpath12
  (is (c/bench (dotimes [n 1] (pairwise beatpath12)))))

(def beatpath12-res (pairwise beatpath12))

(defbench condorcet-beatpath12
  (is (c/bench (dotimes [n 1] (condorcet beatpath12-res)))))

(defbench weak-condorcet-beatpath12
  (is (c/bench (dotimes [n 1] (weak-condorcet beatpath12-res)))))

(defbench smith-set-beatpath12
  (is (c/bench (dotimes [n 1] (smith-set beatpath12-res)))))

(defbench schwartz-set-beatpath12
  (is (c/bench (dotimes [n 1] (schwartz-set beatpath12-res)))))

(defbench llull-copeland-beatpath12
  (is (c/bench (dotimes [n 1] (copeland beatpath12-res)))))

(defbench copeland-beatpath12
  (is (c/bench (dotimes [n 1] (copeland beatpath12-res)))))

(defbench ranked-pairs-beatpath12
  (is (c/bench (dotimes [n 1] (ranked-pairs beatpath12-res)))))

(defbench schulze-beatpath12
  (is (c/bench (dotimes [n 1] (schulze beatpath12-res)))))
