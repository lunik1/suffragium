(ns suffragium.pairwise-test
  (:require [clojure.test :refer :all]
            [clojure.math.combinatorics :refer [permuted-combinations]]
            [suffragium.pairwise :refer :all]
            [suffragium.util :refer :all]
            [suffragium.loom-util :refer :all]
            [suffragium.example-ballots :refer :all]))

;; https://electowiki.org/wiki/Beatpath_examples_3
(def condorcet-3 [{:a 1 :b 2 :c 3}]) ; a -> b -> c <- a
(def weak-condorcet-3 [{:a 1 :b 2 :c 3} {:a 3 :b 1 :c 2} {:a 1 :b 2 :c 1}]) ; a -> b -> c -- a
(def leader-tie-3 [{:a 1 :b 1 :c 2}]) ; a -- b -> c <- a
(def tie-3 [{:a 1 :b 1 :c 1}]) ; a -- b -- c -- a
(def cycle-3 [{:a 1 :b 2 :c 3} {:a 3 :b 1 :c 2} {:a 2 :b 3 :c 1}]) ; a -> b -> c -> a

(def weak-condorcet-tie-3 [{:a 1 :b 2 :c 3} {:a 2 :b 2 :c 1}]) ; a -> b -- c -- a
(def loser-tie-3 [{:a 1 :b 2 :c 2}]) ; a -> b -- c <- a

(deftest make-pairwise-tally-test
  (let [make-pairwise-tally #'suffragium.pairwise/make-pairwise-tally]
    (is (= {#{:a :b} {:winner :suffragium.pairwise/tie :margin 0}
            #{:a :c} {:winner :suffragium.pairwise/tie :margin 0}
            #{:b :c} {:winner :suffragium.pairwise/tie :margin 0}}
           (make-pairwise-tally '(:a :b :c))))))

(deftest pairwise-edges-test
  (let [pairwise-edges #'suffragium.pairwise/pairwise-edges
        pairwise-impl #'suffragium.pairwise/pairwise-impl]
    (is (= [[:a :b 1]]
           (-> [{:a 1 :b 2}] pairwise-impl pairwise-edges)))
    (is (freq= [[:a :b 0] [:b :a 0]]
               (-> [{:a 1 :b 1}] pairwise-impl pairwise-edges)))
    (is (freq= [[:a :b 1] [:b :c 1] [:a :c 1]]
               (-> condorcet-3 pairwise-impl pairwise-edges)))
    (is (freq= [[:a :b 1] [:b :c 1] [:a :c 0] [:c :a 0]]
               (-> weak-condorcet-3 pairwise-impl pairwise-edges)))
    (is (freq= [[:a :b 0] [:b :a 0] [:a :c 1] [:b :c 1]]
               (-> leader-tie-3 pairwise-impl pairwise-edges)))
    (is (freq= [[:a :b 0] [:b :a 0] [:a :c 0] [:c :a 0] [:b :c 0] [:c :b 0]]
               (-> tie-3 pairwise-impl pairwise-edges)))
    (is (freq= [[:a :b 1] [:c :a 1] [:b :c 1]]
               (-> cycle-3 pairwise-impl pairwise-edges)))
    (is (freq= [[:a :b 1] [:b :c 0] [:c :b 0] [:a :c 0] [:c :a 0]]
               (-> weak-condorcet-tie-3 pairwise-impl pairwise-edges)))
    (is (freq= [[:a :b 1] [:b :c 0] [:c :b 0] [:a :c 1]]
               (-> loser-tie-3 pairwise-impl pairwise-edges)))
    (is (freq= [[:orange :purple 17]
                [:red :green 19]
                [:orange :green 19]
                [:red :purple 31]
                [:orange :blue 11]
                [:orange :red 1]
                [:purple :green 19]
                [:blue :green 19]
                [:purple :blue 23]
                [:red :blue 3]]
               (-> pbs pairwise-impl pairwise-edges)))
    (is (freq= [[:A :B  6] [:A :C  0] [:C :A  0] [:D :A  2] [:A :E  0] [:E :A  0]
                [:A :F  0] [:F :A  0] [:A :G  0] [:G :A  0] [:A :H  0] [:H :A  0]
                [:A :I  0] [:I :A  0] [:A :J  0] [:J :A  0] [:A :K  0] [:K :A  0]
                [:A :L  0] [:L :A  0] [:B :C  8] [:B :D  4] [:B :E  0] [:E :B  0]
                [:B :F  8] [:B :G  0] [:G :B  0] [:B :H  0] [:H :B  0] [:B :I 22]
                [:B :J 22] [:B :K 22] [:B :L 22] [:C :D  8] [:C :E  0] [:E :C  0]
                [:C :F  0] [:F :C  0] [:C :G  0] [:G :C  0] [:C :H  0] [:H :C  0]
                [:C :I 20] [:C :J 20] [:C :K 20] [:C :L 20] [:D :E  0] [:E :D  0]
                [:D :F  0] [:F :D  0] [:D :G  8] [:D :H  0] [:H :D  0] [:D :I 18]
                [:D :J 18] [:D :K 18] [:D :L 18] [:E :F  0] [:F :E  0] [:E :G  0]
                [:G :E  0] [:E :H  2] [:E :I 12] [:E :J 12] [:E :K 12] [:E :L 12]
                [:F :G  0] [:G :F  0] [:F :H  9] [:F :I 14] [:F :J 14] [:F :K 14]
                [:F :L 14] [:G :H  9] [:G :I 14] [:G :J 14] [:G :K 14] [:G :L 14]
                [:H :I 17] [:H :J 17] [:H :K 17] [:H :L 17] [:J :I  1] [:I :K  1]
                [:I :L  0] [:K :J  1] [:L :J  0] [:J :L  0] [:L :I  0] [:K :L  0]
                [:L :K  0]]
               (-> beatpath12 pairwise-impl pairwise-edges)))))

(deftest invert-test
  (let [pairwise-edges #'suffragium.pairwise/pairwise-edges
        pairwise-impl #'suffragium.pairwise/pairwise-impl]
    (is (= [[:b :a 1]]
           (-> [{:a 1 :b 2}] pairwise invert wedges)))
    (is (freq= [[:b :a 0] [:a :b 0]]
               (-> [{:a 1 :b 1}] pairwise invert wedges)))
    (is (freq= [[:b :a 1] [:c :b 1] [:c :a 1]]
               (-> condorcet-3 pairwise invert wedges)))
    (is (freq= [[:b :a 1] [:c :b 1] [:c :a 0] [:a :c 0]]
               (-> weak-condorcet-3 pairwise invert wedges)))
    (is (freq= [[:b :a 0] [:a :b 0] [:c :a 1] [:c :b 1]]
               (-> leader-tie-3 pairwise invert wedges)))
    (is (freq= [[:b :a 0] [:a :b 0] [:c :a 0] [:a :c 0] [:c :b 0] [:b :c 0]]
               (-> tie-3 pairwise invert wedges)))
    (is (freq= [[:b :a 1] [:a :c 1] [:c :b 1]]
               (-> cycle-3 pairwise invert wedges)))
    (is (freq= [[:b :a 1] [:c :b 0] [:b :c 0] [:c :a 0] [:a :c 0]]
               (-> weak-condorcet-tie-3 pairwise invert wedges)))
    (is (freq= [[:b :a 1] [:c :b 0] [:b :c 0] [:c :a 1]]
               (-> loser-tie-3 pairwise invert wedges)))
    (is (freq= [[:purple :orange 17]
                [:green :red 19]
                [:green :orange 19]
                [:purple :red 31]
                [:blue :orange 11]
                [:red :orange 1]
                [:green :purple 19]
                [:green :blue 19]
                [:blue :purple 23]
                [:blue :red 3]]
               (-> pbs pairwise invert wedges)))
    (is (freq= [[:B :A  6] [:C :A  0] [:A :C  0] [:A :D  2] [:E :A  0] [:A :E  0]
                [:F :A  0] [:A :F  0] [:G :A  0] [:A :G  0] [:H :A  0] [:A :H  0]
                [:I :A  0] [:A :I  0] [:J :A  0] [:A :J  0] [:K :A  0] [:A :K  0]
                [:L :A  0] [:A :L  0] [:C :B  8] [:D :B  4] [:E :B  0] [:B :E  0]
                [:F :B  8] [:G :B  0] [:B :G  0] [:H :B  0] [:B :H  0] [:I :B 22]
                [:J :B 22] [:K :B 22] [:L :B 22] [:D :C  8] [:E :C  0] [:C :E  0]
                [:F :C  0] [:C :F  0] [:G :C  0] [:C :G  0] [:H :C  0] [:C :H  0]
                [:I :C 20] [:J :C 20] [:K :C 20] [:L :C 20] [:E :D  0] [:D :E  0]
                [:F :D  0] [:D :F  0] [:G :D  8] [:H :D  0] [:D :H  0] [:I :D 18]
                [:J :D 18] [:K :D 18] [:L :D 18] [:F :E  0] [:E :F  0] [:G :E  0]
                [:E :G  0] [:H :E  2] [:I :E 12] [:J :E 12] [:K :E 12] [:L :E 12]
                [:G :F  0] [:F :G  0] [:H :F  9] [:I :F 14] [:J :F 14] [:K :F 14]
                [:L :F 14] [:H :G  9] [:I :G 14] [:J :G 14] [:K :G 14] [:L :G 14]
                [:I :H 17] [:J :H 17] [:K :H 17] [:L :H 17] [:I :J  1] [:K :I  1]
                [:L :I  0] [:J :K  1] [:J :L  0] [:L :J  0] [:I :L  0] [:L :K  0]
                [:K :L  0]]
               (-> beatpath12 pairwise invert wedges)))))

(deftest inc-pairwise-test
  (let [inc-pairwise #'suffragium.pairwise/inc-pairwise]
    (is (= {:winner :a :margin 100} (inc-pairwise {:winner :a :margin 99} :a)))
    (is (= {:winner :a :margin 100} (inc-pairwise {:winner :a :margin 101} :b)))
    (is (= {:winner :a :margin 1} (inc-pairwise {:winner :suffragium.pairwise/tie :margin 0} :a)))
    (is (= {:winner :suffragium.pairwise/tie :margin 0} (inc-pairwise {:winner :a :margin 1} :b)))))

(deftest pairwise-tally-test
  (let [pairwise-tally #'suffragium.pairwise/pairwise-tally
        make-pairwise-tally #'suffragium.pairwise/make-pairwise-tally]
    (let [t (make-pairwise-tally [:a :b :c])]
      (is (= t (pairwise-tally t {:a 1 :b 1 :c 1}))))
    (is (= {#{:a :b} {:winner :a :margin 1}
            #{:a :c} {:winner :a :margin 1}
            #{:b :c} {:winner :b :margin 1}}
           (pairwise-tally (make-pairwise-tally [:a :b :c]) {:a 1 :b 2 :c 3})))
    (is (= {#{:a :b} {:winner :a :margin 1}
            #{:a :c} {:winner :a :margin 1}
            #{:b :c} {:winner :suffragium.pairwise/tie :margin 0}}
           (pairwise-tally (make-pairwise-tally [:a :b :c]) {:a 1 :b 2 :c 2})))))

(deftest condorcet-test
  (is (= :a (condorcet [{:a 1}])))
  (is (= :a (condorcet condorcet-3)))
  (is (= :a (condorcet loser-tie-3)))
  (is (= :orange (condorcet pbs) (-> pbs pairwise condorcet)))
  (is (= :Nashville (condorcet tennessee) (-> tennessee pairwise condorcet)))
  (is (nil? (condorcet weak-condorcet-3)))
  (is (nil? (condorcet leader-tie-3)))
  (is (nil? (condorcet tie-3)))
  (is (nil? (condorcet cycle-3)))
  (is (nil? (condorcet weak-condorcet-tie-3)))
  (is (nil? (condorcet paradox)))
  (is (nil? (condorcet beatpath12)))
  (is (nil? (condorcet {})))
  (is (nil? (condorcet nil))))

(deftest weak-condorcet-test
  (is (= #{:a} (weak-condorcet [{:a 1}])))
  (is (= #{:a} (weak-condorcet condorcet-3)))
  (is (= #{:a} (weak-condorcet weak-condorcet-3)))
  (is (= #{:a :b} (weak-condorcet leader-tie-3)))
  (is (= #{:a :b :c} (weak-condorcet tie-3)))
  (is (= #{:a} (weak-condorcet loser-tie-3)))
  (is (= #{:a :c} (weak-condorcet weak-condorcet-tie-3)))
  (is (= #{:orange} (weak-condorcet pbs) (-> pbs pairwise weak-condorcet)))
  (is (= #{:Nashville} (weak-condorcet tennessee) (-> tennessee pairwise weak-condorcet)))
  (is (= #{:E} (weak-condorcet beatpath12) (-> beatpath12 pairwise weak-condorcet)))
  (is (= #{} (weak-condorcet cycle-3)))
  (is (= #{} (weak-condorcet paradox)))
  (is (= #{} (weak-condorcet {})))
  (is (= #{} (weak-condorcet nil))))

(deftest smith-set-test
  (is (= #{:a} (-> [{:a 1}] pairwise smith-set)))
  (is (= #{:a} (-> [{:a 1 :b 2}] pairwise smith-set)))
  (is (= #{:a :b} (-> [{:a 1 :b 1}] pairwise smith-set)))
  (is (= #{:a} (-> condorcet-3 pairwise smith-set)))
  (is (= #{:a :b :c} (-> weak-condorcet-3 pairwise smith-set)))
  (is (= #{:a :b} (-> leader-tie-3 pairwise smith-set)))
  (is (= #{:a :b :c} (-> tie-3 pairwise smith-set)))
  (is (= #{:a :b :c} (-> cycle-3 pairwise smith-set)))
  (is (= #{:a :b :c} (-> weak-condorcet-tie-3 pairwise smith-set)))
  (is (= #{:a} (-> loser-tie-3 pairwise smith-set)))
  (is (= #{:orange} (-> pbs pairwise smith-set)))
  (is (= #{:Nashville} (-> tennessee pairwise smith-set)))
  (is (= #{:A :B :C :D :E} (-> paradox pairwise smith-set)))
  (is (= #{:A :B :C :D :E :F :G :H :I :J :K :L}
         (-> beatpath12 pairwise smith-set))))

(deftest schwartz-components-test
  (is (freq= [#{:a}] (-> [{:a 1}] pairwise schwartz-components)))
  (is (freq= [#{:a}] (-> [{:a 1 :b 2}] pairwise schwartz-components)))
  (is (freq= [#{:a}] (-> [{:a 1 :b 2}] pairwise schwartz-components)))
  (is (freq= [#{:a} #{:b}] (-> [{:a 1 :b 1}] pairwise schwartz-components)))
  (is (freq= [#{:a}] (-> condorcet-3 pairwise schwartz-components)))
  (is (freq= [#{:a}] (-> weak-condorcet-3 pairwise schwartz-components)))
  (is (freq= [#{:a} #{:b}] (-> leader-tie-3 pairwise schwartz-components)))
  (is (freq= [#{:a} #{:b} #{:c}] (-> tie-3 pairwise schwartz-components)))
  (is (freq= [#{:a :b :c}] (-> cycle-3 pairwise schwartz-components)))
  (is (freq= [#{:a} #{:c}] (-> weak-condorcet-tie-3 pairwise schwartz-components)))
  (is (freq= [#{:a}] (-> loser-tie-3 pairwise schwartz-components)))
  (is (freq= [#{:orange}] (-> pbs pairwise schwartz-components)))
  (is (freq= [#{:Nashville}] (-> tennessee pairwise schwartz-components)))
  (is (freq= [#{:A :B :C :D} #{:E}] (-> beatpath12 pairwise schwartz-components))))

(deftest copeland-test
  (let [bs [{}]]
    (is (seq= (array-map)
              (copeland bs)
              (-> bs pairwise copeland))))
  (let [bs [{:a 1}]]
    (is (seq= (array-map :a 0)
              (copeland bs)
              (-> bs pairwise copeland))))
  (let [bs [{:a 1 :b 2}]]
    (is (seq= (array-map :a 1 :b -1)
              (copeland bs)
              (-> bs pairwise copeland))))
  (let [bs [{:a 1 :b 1}]]
    (is (seq= (array-map :b 0 :a 0)
              (copeland bs :tbrc [:b :a])
              (-> bs pairwise (copeland :tbrc [:b :a])))))
  (is (seq= (array-map :a 2 :b 0 :c -2)
            (copeland condorcet-3)
            (-> condorcet-3 pairwise copeland)))
  (is (seq= (array-map :a 1 :b 0 :c -1)
            (copeland weak-condorcet-3)
            (-> weak-condorcet-3 pairwise copeland)))
  (is (seq= (array-map :a 1 :b 1 :c -2)
            (copeland leader-tie-3 :tbrc [:a :b :c])
            (-> leader-tie-3 pairwise (copeland :tbrc [:a :b :c]))))
  (is (seq= (array-map :a 0 :b 0 :c 0)
            (copeland tie-3 :tbrc [:a :b :c])
            (-> tie-3 pairwise (copeland :tbrc [:a :b :c]))))
  (is (seq= (array-map :c 0 :a 0 :b 0)
            (copeland tie-3 :tbrc [:c :a :b])
            (-> tie-3 pairwise (copeland :tbrc [:c :a :b]))))
  (is (seq= (array-map :a 0 :b 0 :c 0)
            (copeland cycle-3 :tbrc [:a :b :c])
            (-> cycle-3 pairwise (copeland :tbrc [:a :b :c]))))
  (is (seq= (array-map :a 1 :c 0 :b -1)
            (copeland weak-condorcet-tie-3)
            (-> weak-condorcet-tie-3 pairwise copeland)))
  (is (seq= (array-map :a 2 :b -1 :c -1)
            (copeland loser-tie-3 :tbrc [:a :b :c])
            (-> loser-tie-3 pairwise (copeland :tbrc [:a :b :c]))))
  (is (seq= (array-map :Nashville 3 :Chattanooga 1 :Knoxville -1 :Memphis -3)
            (copeland tennessee)
            (-> tennessee pairwise copeland)))
  (is (seq= (array-map :A 2 :B 0 :C 0 :E 0 :D -2)
            (copeland paradox :tbrc [:A :B :C :D :E])
            (-> paradox pairwise (copeland :tbrc [:A :B :C :D :E]))))
  (is (seq= (array-map :B 6 :E 5 :C 4 :D 4 :F 4 :G 4 :H 1 :A 0 :I -7 :J -7 :K -7 :L -7)
            (copeland beatpath12 :tbrc [:A :B :C :D :E :F :G :H :I :J :K :L])
            (-> beatpath12 pairwise (copeland :tbrc [:A :B :C :D :E :F :G :H :I :J :K :L])))))

(deftest llull-copeland-test
  (let [bs [{}]]
    (is (seq= (array-map)
              (llull-copeland bs)
              (-> bs pairwise llull-copeland))))
  (let [bs [{:a 1}]]
    (is (seq= (array-map :a 0)
              (llull-copeland bs)
              (-> bs pairwise llull-copeland))))
  (let [bs [{:a 1 :b 2}]]
    (is (seq= (array-map :a 1 :b -0)
              (llull-copeland bs)
              (-> bs pairwise llull-copeland))))
  (let [bs [{:a 1 :b 1}]]
    (is (seq= (array-map :b 0 :a 0)
              (llull-copeland bs :tbrc [:b :a])
              (-> bs pairwise (llull-copeland :tbrc [:b :a])))))
  (is (seq= (array-map :a 2 :b 1 :c 0)
            (llull-copeland condorcet-3)
            (-> condorcet-3 pairwise llull-copeland)))
  (is (seq= (array-map :a 1 :b 1 :c 0)
            (llull-copeland weak-condorcet-3 :tbrc [:a :b :c])
            (-> weak-condorcet-3 pairwise (llull-copeland :tbrc [:a :b :c]))))
  (is (seq= (array-map :a 1 :b 1 :c 0)
            (llull-copeland leader-tie-3 :tbrc [:a :b :c])
            (-> leader-tie-3 pairwise (llull-copeland :tbrc [:a :b :c]))))
  (is (seq= (array-map :a 0 :b 0 :c 0)
            (llull-copeland tie-3 :tbrc [:a :b :c])
            (-> tie-3 pairwise (llull-copeland :tbrc [:a :b :c]))))
  (is (seq= (array-map :c 0 :a 0 :b 0)
            (llull-copeland tie-3 :tbrc [:c :a :b])
            (-> tie-3 pairwise (llull-copeland :tbrc [:c :a :b]))))
  (is (seq= (array-map :a 1 :b 1 :c 1)
            (llull-copeland cycle-3 :tbrc [:a :b :c])
            (-> cycle-3 pairwise (llull-copeland :tbrc [:a :b :c]))))
  (is (seq= (array-map :a 1 :b 0 :c 0)
            (llull-copeland weak-condorcet-tie-3 :tbrc [:a :b :c])
            (-> weak-condorcet-tie-3 pairwise (llull-copeland :tbrc [:a :b :c]))))
  (is (seq= (array-map :a 2 :b 0 :c 0)
            (llull-copeland loser-tie-3 :tbrc [:a :b :c])
            (-> loser-tie-3 pairwise (llull-copeland :tbrc [:a :b :c]))))
  (is (seq= (array-map :Nashville 3 :Chattanooga 2 :Knoxville 1 :Memphis 0)
            (llull-copeland tennessee)
            (-> tennessee pairwise llull-copeland)))
  (is (seq= (array-map :A 3 :B 2 :C 2 :E 2 :D 1)
            (llull-copeland paradox :tbrc [:A :B :C :D :E])
            (-> paradox pairwise (llull-copeland :tbrc [:A :B :C :D :E]))))
  (is (seq= (array-map :B 7 :D 6 :C 5 :E 5 :F 5 :G 5 :H 4 :A 1 :I 1 :J 1 :K 1 :L 0)
            (llull-copeland beatpath12 :tbrc [:A :B :C :D :E :F :G :H :I :J :K :L])
            (-> beatpath12 pairwise (llull-copeland :tbrc [:A :B :C :D :E :F :G :H :I :J :K :L])))))

(deftest cretney-test
  (let [coll [:A :B :C :D :E :F :G]
        cretney #'suffragium.pairwise/cretney]
    (is (= [[:A :G] [:A :F] [:A :E] [:A :D] [:A :C] [:A :B] [:B :G] [:B :F]
            [:B :E] [:B :D] [:B :C] [:B :A] [:C :G] [:C :F] [:C :E] [:C :D]
            [:C :B] [:C :A] [:D :G] [:D :F] [:D :E] [:D :C] [:D :B] [:D :A]
            [:E :G] [:E :F] [:E :D] [:E :C] [:E :B] [:E :A] [:F :G] [:F :E]
            [:F :D] [:F :C] [:F :B] [:F :A] [:G :F] [:G :E] [:G :D] [:G :C]
            [:G :B] [:G :A]]
           (sort (partial cretney coll) (permuted-combinations coll 2))))))

(deftest legrand-test
  (let [coll [:A :B :C :D :E :F :G]
        legrand #'suffragium.pairwise/legrand]
    (is (= [[:A :G] [:B :G] [:C :G] [:D :G] [:E :G] [:F :G] [:A :F] [:B :F]
            [:C :F] [:D :F] [:E :F] [:G :F] [:A :E] [:B :E] [:C :E] [:D :E]
            [:F :E] [:G :E] [:A :D] [:B :D] [:C :D] [:E :D] [:F :D] [:G :D]
            [:A :C] [:B :C] [:D :C] [:E :C] [:F :C] [:G :C] [:A :B] [:C :B]
            [:D :B] [:E :B] [:F :B] [:G :B] [:B :A] [:C :A] [:D :A] [:E :A]
            [:F :A] [:G :A]]
           (sort (partial legrand coll) (permuted-combinations coll 2))))))

(deftest schulze-i-test
  (let [coll [:A :B :C :D :E :F :G]
        schulze-i #'suffragium.pairwise/schulze-i]
    (is (= [[:A :G] [:A :F] [:A :E] [:A :D] [:A :C] [:A :B] [:B :G] [:B :F]
            [:B :E] [:B :D] [:B :C] [:C :G] [:C :F] [:C :E] [:C :D] [:D :G]
            [:D :F] [:D :E] [:E :G] [:E :F] [:F :G] [:B :A] [:C :B] [:C :A]
            [:D :C] [:D :B] [:D :A] [:E :D] [:E :C] [:E :B] [:E :A] [:F :E]
            [:F :D] [:F :C] [:F :B] [:F :A] [:G :F] [:G :E] [:G :D] [:G :C]
            [:G :B] [:G :A]]
           (sort (partial schulze-i coll) (permuted-combinations coll 2))))))

(deftest schulze-ii-test
  (let [coll [:A :B :C :D :E :F :G]
        schulze-ii #'suffragium.pairwise/schulze-ii]
    (is (= [[:A :G] [:B :G] [:C :G] [:D :G] [:E :G] [:F :G] [:A :F] [:B :F]
            [:C :F] [:D :F] [:E :F] [:A :E] [:B :E] [:C :E] [:D :E] [:A :D]
            [:B :D] [:C :D] [:A :C] [:B :C] [:A :B] [:G :F] [:F :E] [:G :E]
            [:E :D] [:F :D] [:G :D] [:D :C] [:E :C] [:F :C] [:G :C] [:C :B]
            [:D :B] [:E :B] [:F :B] [:G :B] [:B :A] [:C :A] [:D :A] [:E :A]
            [:F :A] [:G :A]]
           (sort (partial schulze-ii coll) (permuted-combinations coll 2))))))

(deftest ranked-pairs-test
  (is (seq= (ranked-pairs condorcet-3)
            (-> condorcet-3 pairwise ranked-pairs)
            (array-map :a 2 :b 0 :c -2)))
  (is (seq= (ranked-pairs weak-condorcet-3)
            (-> weak-condorcet-3 pairwise ranked-pairs)
            (array-map :a 2 :b 0 :c -2)))
  (is (seq= (ranked-pairs leader-tie-3)
            (-> leader-tie-3 pairwise ranked-pairs)
            (array-map :a 2 :b 0 :c -2)))
  (is (seq= (ranked-pairs tie-3)
            (-> tie-3 pairwise ranked-pairs)
            (array-map :a 2 :b 0 :c -2)))
  (is (seq= (ranked-pairs tie-3 :tbrc [:c :b :a])
            (-> tie-3 pairwise (ranked-pairs :tbrc [:c :b :a]))
            (array-map :c 2 :b 0 :a -2)))
  (is (seq= (ranked-pairs tie-3 :tbrp [[:a :c] [:c :b] [:a :b] [:b :a] [:c :a] [:b :c]])
            (-> tie-3 pairwise (ranked-pairs :tbrp [[:a :c] [:c :b] [:a :b] [:b :a] [:c :a] [:b :c]]))
            (array-map :a 2 :c 0 :b -2)))
  (is (seq= (ranked-pairs cycle-3)
            (-> cycle-3 pairwise ranked-pairs)
            (array-map :a 1 :b 0 :c -1)))
  (is (seq= (ranked-pairs weak-condorcet-tie-3)
            (-> weak-condorcet-tie-3 pairwise ranked-pairs)
            (array-map :a 2 :b 0 :c -2)))
  (is (seq= (ranked-pairs loser-tie-3)
            (-> loser-tie-3 pairwise ranked-pairs)
            (array-map :a 2 :b 0 :c -2)))
  (is (seq= (ranked-pairs tennessee)
            (-> tennessee pairwise ranked-pairs)
            (array-map :Nashville 3 :Chattanooga 1 :Knoxville -1 :Memphis -3))))

;; https://arxiv.org/ftp/arxiv/papers/1804/1804.02973.pdf
(deftest schulze-test
  (is (= (schulze condorcet-3)
         (schulze condorcet-3 :tbrc [:c :b :a])
         (-> condorcet-3 pairwise schulze)
         '(:a :b :c)))
  (is (= (schulze weak-condorcet-3)
         (-> weak-condorcet-3 pairwise schulze)
         '(:a :b :c)))
  (is (= (schulze weak-condorcet-3)
         (-> weak-condorcet-3 pairwise schulze)
         '(:a :b :c)))
  (is (= (schulze leader-tie-3)
         (-> leader-tie-3 pairwise schulze)
         '(:a :b :c)))
  (is (= (schulze tie-3)
         (-> tie-3 pairwise schulze)
         '(:a :b :c)))
  (is (= (schulze cycle-3)
         (-> cycle-3 pairwise schulze)
         '(:a :b :c)))
  (is (= (schulze cycle-3 :tbrc [:b :a :c])
         (-> cycle-3 pairwise (schulze :tbrc [:b :a :c]))
         '(:b :a :c)))
  (is (= (schulze weak-condorcet-tie-3)
         (-> weak-condorcet-tie-3 pairwise schulze)
         '(:a :b :c)))
  (is (= (schulze loser-tie-3)
         (-> loser-tie-3 pairwise schulze)
         '(:a :b :c)))
  (let [s1 (concat (take 8 (repeat {:a 1 :c 2 :d 3 :b 4}))
                   (take 2 (repeat {:b 1 :a 2 :d 3 :c 4}))
                   (take 4 (repeat {:c 1 :d 2 :b 3 :a 4}))
                   (take 4 (repeat {:d 1 :b 2 :a 3 :c 4}))
                   (take 3 (repeat {:d 1 :c 2 :b 3 :a 4})))]
    (is (= (schulze s1) '(:d :a :c :b))))
  (let [s2 (concat (take 3 (repeat {:a 1 :c 2 :d 3 :b 4}))
                   (take 9 (repeat {:b 1 :a 2 :c 3 :d 4}))
                   (take 8 (repeat {:c 1 :d 2 :a 3 :b 4}))
                   (take 5 (repeat {:d 1 :a 2 :b 3 :c 4}))
                   (take 5 (repeat {:d 1 :b 2 :c 3 :a 4})))]
    (is (= (schulze s2) '(:c :d :b :a))))
  (let [s3 (concat (take 5 (repeat {:a 1 :c 2 :b 3 :e 4 :d 5}))
                   (take 5 (repeat {:a 1 :d 2 :e 3 :c 4 :b 5}))
                   (take 8 (repeat {:b 1 :e 2 :d 3 :a 4 :c 5}))
                   (take 3 (repeat {:c 1 :a 2 :b 3 :e 4 :d 5}))
                   (take 7 (repeat {:c 1 :a 2 :e 3 :b 4 :d 5}))
                   (take 2 (repeat {:c 1 :b 2 :a 3 :d 4 :e 5}))
                   (take 7 (repeat {:d 1 :c 2 :e 3 :b 4 :a 5}))
                   (take 8 (repeat {:e 1 :b 2 :a 3 :d 4 :c 5})))]
    (is (= (schulze s3) '(:e :a :c :b :d))))
  (let [s4 (concat (take 3 (repeat {:a 1 :b 2 :c 3 :d 4}))
                   (take 2 (repeat {:c 1 :b 2 :d 3 :a 4}))
                   (take 2 (repeat {:d 1 :a 2 :b 3 :c 4}))
                   (take 2 (repeat {:d 1 :b 2 :c 3 :a 4})))]
    (is (= (schulze s4) (schulze s4 :tbrp :legrand) '(:b :c :d :a)))
    (is (= (schulze s4 :tbrp :zavist) '(:b :d :c :a)))
    (is (= (schulze s4 :tbrp :schulze-i) (schulze s4 :tbrp :cretney) '(:d :a :b :c)))
    (is (= (schulze s4 :tbrc [:d :b :c :a]) '(:d :b :c :a))))
  (let [s5 (concat (take 12 (repeat {:a 1 :b 2 :c 3 :d 4}))
                   (take  6 (repeat {:a 1 :d 2 :b 3 :c 4}))
                   (take  9 (repeat {:b 1 :c 2 :d 3 :a 4}))
                   (take 15 (repeat {:c 1 :d 2 :a 3 :b 4}))
                   (take 21 (repeat {:d 1 :b 2 :a 3 :c 4})))]
    (is (= (schulze s5) '(:d :a :b :c))))
  (let [s6 (concat (take 6 (repeat {:a 1 :c 2 :d 3 :b 4}))
                   (take 1 (repeat {:b 1 :a 2 :d 3 :c 4}))
                   (take 3 (repeat {:c 1 :b 2 :d 3 :a 4}))
                   (take 3 (repeat {:d 1 :b 2 :a 3 :c 4}))
                   (take 2 (repeat {:d 1 :c 2 :b 3 :a 4})))]
    (is (= (schulze s6) '(:a :c :d :b))))
  (let [s7 (concat (take 3 (repeat {:a 1 :d 2 :e 3 :b 4 :c 5 :f 6}))
                   (take 3 (repeat {:b 1 :f 2 :e 3 :c 4 :d 5 :a 6}))
                   (take 4 (repeat {:c 1 :a 2 :b 3 :f 4 :d 5 :e 6}))
                   (take 1 (repeat {:d 1 :b 2 :c 3 :e 4 :f 5 :a 6}))
                   (take 4 (repeat {:d 1 :e 2 :f 3 :a 4 :b 5 :c 6}))
                   (take 2 (repeat {:e 1 :c 2 :b 3 :d 4 :f 5 :a 6}))
                   (take 2 (repeat {:f 1 :a 2 :c 3 :d 4 :b 5 :e 6})))]
    (is (= (schulze s7) '(:a :b :f :d :e :c)))
    (is (= (schulze (concat s7 (take 2 (repeat {:a 1 :e 2 :f 3 :c 4 :b 5 :d 6}))))
           '(:d :e :c :a :b :f))))
  (let [s8o (concat (take 3 (repeat {:a 1 :b 2 :d 3 :c 4}))
                    (take 5 (repeat {:a 1 :d 2 :b 3 :c 4}))
                    (take 1 (repeat {:a 1 :d 2 :c 3 :b 4}))
                    (take 2 (repeat {:b 1 :a 2 :d 3 :c 4}))
                    (take 2 (repeat {:b 1 :d 2 :c 3 :a 4}))
                    (take 4 (repeat {:c 1 :a 2 :b 3 :d 4}))
                    (take 6 (repeat {:c 1 :b 2 :a 3 :d 4}))
                    (take 2 (repeat {:d 1 :b 2 :c 3 :a 4}))
                    (take 5 (repeat {:d 1 :c 2 :a 3 :b 4})))
        s8n (concat (take 3 (repeat {:a 1 :b 2 :d 3 :e 4 :c 5}))
                    (take 5 (repeat {:a 1 :d 2 :e 3 :b 4 :c 5}))
                    (take 1 (repeat {:a 1 :d 2 :e 3 :c 4 :b 5}))
                    (take 2 (repeat {:b 1 :a 2 :d 3 :e 4 :c 5}))
                    (take 2 (repeat {:b 1 :d 2 :e 3 :c 4 :a 5}))
                    (take 4 (repeat {:c 1 :a 2 :b 3 :d 4 :e 5}))
                    (take 6 (repeat {:c 1 :b 2 :a 3 :d 4 :e 5}))
                    (take 2 (repeat {:d 1 :b 2 :e 3 :c 4 :a 5}))
                    (take 5 (repeat {:d 1 :e 2 :c 3 :a 4 :b 5})))]
    (is (= (schulze s8o) '(:a :d :c :b)))
    (is (= (schulze s8n) '(:b :a :d :e :c))))
  (let [s9o (concat (take 5 (repeat {:a 1 :c 2 :d 3 :b 4}))
                    (take 2 (repeat {:b 1 :c 2 :d 3 :a 4}))
                    (take 4 (repeat {:b 1 :d 2 :a 3 :c 4}))
                    (take 2 (repeat {:c 1 :d 2 :a 3 :b 4})))
        s9n (concat (take 5 (repeat {:a 1 :e 2 :c 3 :d 4 :b 5}))
                    (take 2 (repeat {:b 1 :c 2 :d 3 :a 4 :e 5}))
                    (take 4 (repeat {:b 1 :d 2 :a 3 :e 4 :c 5}))
                    (take 2 (repeat {:c 1 :d 2 :a 3 :b 4 :e 5})))]
    (is (= (schulze s9o) '(:a :c :d :b)))
    (is (= (schulze s9n) '(:b :a :e :c :d))))
  (let [s10 (concat (take  6 (repeat {:a 1 :b 2 :c 3 :d 4}))
                    (take  8 (repeat {:a 1 :b 1 :c 2 :d 2}))
                    (take  8 (repeat {:a 1 :c 1 :b 2 :d 2}))
                    (take 18 (repeat {:a 1 :c 1 :d 2 :b 3}))
                    (take  8 (repeat {:a 1 :c 1 :d 1 :b 2}))
                    (take 40 (repeat {:b 1 :a 2 :c 2 :d 2}))
                    (take  4 (repeat {:c 1 :b 2 :d 3 :a 4}))
                    (take  9 (repeat {:c 1 :d 2 :a 3 :b 4}))
                    (take  8 (repeat {:c 1 :d 1 :a 2 :b 2}))
                    (take 14 (repeat {:d 1 :a 2 :b 3 :c 4}))
                    (take 11 (repeat {:d 1 :b 2 :c 3 :a 4}))
                    (take  4 (repeat {:d 1 :c 2 :a 3 :b 4})))]
    (is (= (schulze s10) '(:a :b :c :d))))
  (let [s11 (concat (take  9 (repeat {:a 1 :d 2 :b 3 :e 4 :c 5}))
                    (take  6 (repeat {:b 1 :c 2 :a 3 :d 4 :e 5}))
                    (take  5 (repeat {:b 1 :c 2 :d 3 :e 4 :a 5}))
                    (take  2 (repeat {:c 1 :d 2 :b 3 :e 4 :a 5}))
                    (take  6 (repeat {:d 1 :e 2 :c 3 :b 4 :a 5}))
                    (take 14 (repeat {:e 1 :a 2 :c 3 :b 4 :d 5}))
                    (take  2 (repeat {:e 1 :c 2 :a 3 :b 4 :d 5}))
                    (take  1 (repeat {:e 1 :d 2 :a 3 :c 4 :b 5})))]
    (is (= (schulze s11) '(:b :e :a :c :d))))
  (let [s12 (concat (take  9 (repeat {:a 1 :d 2 :b 3 :e 4 :c 5}))
                    (take  1 (repeat {:b 1 :a 2 :c 3 :e 4 :d 5}))
                    (take  6 (repeat {:c 1 :b 2 :a 3 :d 4 :e 5}))
                    (take  2 (repeat {:c 1 :d 2 :b 3 :e 4 :a 5}))
                    (take  5 (repeat {:c 1 :d 2 :e 3 :a 4 :b 5}))
                    (take  6 (repeat {:d 1 :e 2 :c 3 :a 4 :b 5}))
                    (take 14 (repeat {:e 1 :b 2 :a 3 :c 4 :d 5}))
                    (take  2 (repeat {:e 1 :b 2 :c 3 :a 4 :d 5})))]
    (is (= (schulze s12) '(:e :a :c :d :b))))
  (let [s13 (concat (take 2 (repeat {:a 1 :b 2 :c 3}))
                    (take 2 (repeat {:b 1 :c 2 :a 3}))
                    (take 1 (repeat {:c 1 :a 2 :b 3})))]
    (is (= (schulze s13) '(:a :b :c)))))
