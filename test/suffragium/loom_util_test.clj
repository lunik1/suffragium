(ns suffragium.loom-util-test
  (:require [loom.graph :refer [weighted-digraph]]
            [clojure.test :refer :all]
            [suffragium.util :refer [freq=]]
            [suffragium.loom-util :refer :all]))

(deftest rev-edge-test
  (is (= [:a :b] (rev-edge [:b :a])))
  (is (= [:a :a] (rev-edge [:a :a])))
  (is (= [:a :b 8] (rev-edge [:b :a 8])))
  (is (= [:a :a 1] (rev-edge [:a :a 1]))))

(deftest wedges-test
  (let [es []]
    (is (freq= es (->> es (apply weighted-digraph) wedges))))
  (let [es [[:a :b 10]]]
    (is (freq= es (->> es (apply weighted-digraph) wedges))))
  (let [es [[:a :b 0] [:b :a 0]]]
    (is (freq= es (->> es (apply weighted-digraph) wedges))))
  (let [es [[:a :b 0] [:b :a 0] [:c :a 5] [:x :y 77]]]
    (is (freq= es (->> es (apply weighted-digraph) wedges)))))
