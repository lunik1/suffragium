(ns suffragium.ballot-util-test
  (:require [clojure.test :refer :all]
            [suffragium.ballot-util :refer :all]))

(deftest top-ranked-test
  (is (= :a (top-ranked {:a 1 :b 2})))
  (is (nil? (top-ranked {})))
  (is (= :b (top-ranked {:a 3 :b 0 :c 32}))))

(deftest pairwise-preferred-test
  (is (= :a (pairwise-preferred {:a 1 :b 2} #{:a :b})))
  (is (= :b (pairwise-preferred {:a 2 :b 1} #{:a :b})))
  (is (nil? (pairwise-preferred {:a 1 :b 1} #{:a :b})))
  (is (= :b (pairwise-preferred {:a 1 :b 2 :c 3} #{:b :c}))))
