(ns suffragium.cardinal-test
  (:require [clojure.test :refer :all]
            [suffragium.util :refer :all]
            [suffragium.cardinal :refer :all]
            [suffragium.example-ballots :refer :all]))

(deftest approval-test
  (is (seq= (array-map) (approval [])))
  (is (seq= (array-map :Burger 6 :Veggie 3 :Steak 2)
            (approval dinner)))
  (is (seq= (array-map :wall-e 4 :terminator2 3 :spirited-away 3)
            (approval movie))))

(deftest plurality-test
  (is (seq= (array-map) (plurality [])))
  (is (seq= (array-map :Memphis 42 :Nashville 26 :Knoxville 17 :Chattanooga 15)
            (plurality tennessee))))

(deftest borda-count-test
  (is (seq= (array-map :green 203, :blue 174, :purple 168, :orange 141, :red 139)
            (borda-count pbs)))
  (is (seq= (array-map :red 136 :orange 134 :purple 107 :blue 101 :green 72)
            (borda-count pbs :score-fn #(- 5 %))))
  (is (seq= (array-map :red 191 :orange 189 :purple 162 :blue 156 :green 127)
            (borda-count pbs :score-fn #(- 6 %))))
  (is (seq= (array-map "t" 1 \b 1 :r 1 'c 1)
            (borda-count [{"t" 1 \b 1 :r 1 'c 1}] :tbrc ["t" \b :r 'c])))
  (is (seq= (array-map) (borda-count []))))

;; https://rangevoting.org/StarVoting.html
(deftest star-test
  (let [bs (concat (take 4 (repeat {'A 5 'B 1 'C 0}))
                   (take 5 (repeat {'B 5 'C 1 'A 0})))]
    (is (seq= (star bs)
              (array-map 'B 29 'A 20 'C 5))))
  (let [bs (concat (take  9 (repeat {:a 5 :b 1 :c 0}))
                   (take 12 (repeat {:b 5 :c 1 :a 0}))
                   (take  8 (repeat {:c 5 :a 1 :b 0})))]
    (is (seq= (star bs)
              (array-map :a 53 :b 69 :c 52))))
  (let [bs (concat (take  9 (repeat {:a 5 :b 1 :c 0}))
                   (take  7 (repeat {:b 5 :c 1 :a 0}))
                   (take  5 (repeat {:a 5 :b 0 :c 0}))
                   (take  8 (repeat {:c 5 :a 1 :b 0})))]
    (is (seq= (star bs)
              (array-map :c 47 :a 78 :b 44))))
  (let [bs (concat (take 23 (repeat {\B 2 \C 1 \A 0}))
                   (take 16 (repeat {\A 2 \C 1 \B 0}))
                   (take 11 (repeat {\C 2 \A 1 \B 0}))
                   (take 10 (repeat {\A 2 \B 1 \C 0})))]
    (is (seq= (star bs)
              (array-map \C 61 \A 63 \B 56))))
  (is (seq= (star [{"b" 1 'c 1 :a 1 \d 1 ::z 1}] :tbrc [:a "b" 'c \d ::z])
            (array-map :a 1 "b" 1 'c 1 \d 1 ::z 1)))
  (is (seq= (star [{"Alex" 1 "Bob" 2 "Catherine" 4}] :score-fn #(* % %))
            (array-map "Catherine" 16 "Bob" 4 "Alex" 1))))
