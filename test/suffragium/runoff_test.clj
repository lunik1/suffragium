(ns suffragium.runoff-test
  (:require [clojure.test :refer :all]
            [suffragium.example-ballots :refer :all]
            [suffragium.util :refer :all]
            [suffragium.runoff :refer :all]))

(deftest irv-test
  (is (seq= (irv tennessee)
            {:Knoxville [17 32 58],
             :Memphis [42 42 42],
             :Nashville [26 26],
             :Chattanooga [15]}))
  (is (seq= (irv pbs)
            {:purple [10 12 21 37],
             :green [18 18 18 18],
             :blue [12 16 16],
             :red [9 9],
             :orange [6]}))
  (is (seq= (irv pbs :all-rounds true)
            {:purple [10 12 21 37 55],
             :green [18 18 18 18],
             :blue [12 16 16],
             :red [9 9],
             :orange [6]}))
  (let [bs (concat (take 5 (repeat {:a 1 :b 2 :c 3}))
                   (take 3 (repeat {:b 1 :c 2 :a 3}))
                   (take 3 (repeat {:c 1 :b 2 :a 3})))]
    (is (seq= (irv bs)
              (irv bs :tbrc [:a :b :c])
              {:b [3 6]
               :a [5 5]
               :c [3]}))
    (is (seq= (irv bs :tbrc [:c :b :a])
              {:c [3 6]
               :a [5 5]
               :b [3]}))))
