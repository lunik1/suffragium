(ns suffragium.util-test
  (:require [clojure.test :refer :all]
            [suffragium.util :refer :all]))

(deftest unwrap-set-test
  (is (nil? (unwrap-set #{})))
  (is (= :a (unwrap-set #{:a})))
  (is (nil? (unwrap-set #{nil}))))

(deftest rvec-test
  (is ((every-pred vector? (partial = [])) (rvec [])))
  (is ((every-pred vector? (partial = [:a])) (rvec [:a])))
  (is ((every-pred vector? (partial = [:a :b :c])) (rvec [:c :b :a]))))

(deftest cont=-test
  (is (freq= [] []))
  (is (freq= [] '()))
  (is (freq= [:a] '(:a)))
  (is (freq= [:a :b :c] [:b :c :a]))
  (is (freq= [:a :b :c] '(:b :c :a)))
  (is (freq= [:a :b :c] #{:a :b :c}))
  (is (freq= [:a :a :b] [:a :b :a]))
  (is (not (freq= [:a :b :c] [:x :y :z])))
  (is (not (freq= [:a :b :c] [:a :b :z])))
  (is (not (freq= [:a :a :b :c] [:a :b :c])))
  (is (not (freq= [:a :a :b :c] [:a :b :b :c])))
  (is (freq= [:a :b :c] '(:a :b :c) #{:a :b :c} (lazy-cat [:a] [:b] [:c])))
  (is (not (freq= [:a :b :c] [:a :b :c] [:a :b :z]))))

(deftest index-of-test
  (is (nil? (index-of [] :a)))
  (is (= 0 (index-of [:a] :a)))
  (is (= 2 (index-of [:a :b :c] :c)))
  (is (= 0 (index-of [:a :a :a] :a)))
  (is (nil? (index-of [:a :b :c] :z)))
  (is (= 3 (index-of [[] + 0 nil false nil '(2 1 99) nil nil] nil))))

(deftest before?-test
  (is (not (before? :a :b [])))
  (is (before? :a :b [:a]))
  (is (not (before? :a :b [:b])))
  (is (before? :c :e [:a :b :c :d :e]))
  (is (not (before? :d :b [:a :b :c :d])))
  (is (before? :a :b [:a :b :a]))
  (is (not (before? :a :b [:b :a :b])))
  (is (not (before? :a :a [:a :b])))
  (is (before? "x$'h#" some [#".+" ::april "x$'h#" 18 nil some 47/4 '(#{})])))
