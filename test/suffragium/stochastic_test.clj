(ns suffragium.stochastic-test
  (:require [clojure.test :refer :all]
            [suffragium.stochastic :refer :all]
            [suffragium.util :refer :all]
            [suffragium.example-ballots :refer :all]))

;; Not a very heplful test, it's hard to test randomness...
(deftest ssv-test
  (is (freq= (-> tennessee first keys) (ssv tennessee)))
  (is (= (ssv [{:a 1 ::b 2 \d 4 "c" 3}])
         '(:a ::b "c" \d))))
