(ns suffragium.cardinal
  (:require [suffragium.ballot-util :refer [map-ballot top-ranked]]
            [suffragium.util :refer [into-sorted-array-map before?]]
            [suffragium.pairwise :refer [pairwise invert]]
            [loom.graph :refer [nodes]]
            [medley.core :refer [map-vals]]))

(def ^:private score-result-comp #'suffragium.util/score-result-comp)

(defn- borda-tally
  "Add the values in ballot to the values in tally. If a key is missing, the
  value is treated as 0."
  [tally ballot]
  (merge-with + tally ballot))

(defn borda-count
  "Perform a Borda count election. Returns an array map of candidate keys
  and total score values, ordered from most to least preferred.

  Each ballot should contain a key for each candidate.

  :score-fn is a scoring function that translates the value assigned to each
  candidate on a ballot into a numerical score; defaults to identity.

  :tbrc is a tie-breaking order of the candidates, used to resolve the final
  order in the case of score ties. Should be a vector of all candidates from
  most to least preferred. No guarantees are made about the default TBRC, other
  than completeness.

  Note: this function can be used for range voting, but will not ensure the
  scores are within the allowed range."
  [ballots & {:keys [score-fn tbrc]
              :or {score-fn identity}}]
  (let [tbrc (or tbrc (-> ballots first keys))]
    (transduce (map (partial map-ballot score-fn))
               (completing borda-tally (partial into-sorted-array-map
                                                (partial score-result-comp tbrc)))
               {}
               ballots)))

(defn approval
  "Perform an approval election. Returns an array map of candidate keys and
  number of ballots which approved of them values, in descending order. The
  ordering of candidates with the same number of approved ballots is not
  specified.

  Each ballot should contain a key for each candidate. A truthy value indicates
  approval and a falsey value disapproval.

  :tbrc is a tie-breaking order of the candidates, used to resolve the final
  order in the case of ties. Should be a vector of all candidates from most to
  least preferred. No guarantees are made about the default TBRC, other than
  completeness."
  [ballots & {:keys [tbrc]}]
  (let [tbrc (or tbrc (-> ballots first keys))]
    (borda-count ballots :score-fn #(if (boolean %) 1 0) :tbrc tbrc)))

(defn plurality
  "Perform a plurality (first-past-the-post) election. Returns an array map of
  candidate keys and the number of ballots in which they were top ranked
  values, in descending order.

  A lower vaue on a ballot corresponds to a higher rank. If there are multiple
  candidates with the same lowest value the candidate awarded the vote is not
  specified (however it will be one of the lowest ranked candidates).

  :tbrc is a tie-breaking order of the candidates, used to resolve the final
  order in the case of ties. Should be a vector of all candidates from most to
  least preferred. No guarantees are made about the default TBRC, other than
  completeness."
  [ballots & {:keys [tbrc]}]
  (let [tbrc (or tbrc (-> ballots first keys))]
    (into-sorted-array-map
     (partial score-result-comp tbrc)
     (->> ballots (group-by top-ranked) (map-vals count)))))

(defn- star-result-comp
  "Used to order the results of a star election. Checks if candidates were
  in the final runoff: any candidate in the runoff is ranked higher than a
  candidate not in the runoff. Ranks the winner of the runoff highest, followed
  by the loser of the runoff, and then the remaining candidates by score."
  [pg tbrc [k1 v1] [k2 v2]]
  (let [runoff-cands (nodes pg)]
    (case [(contains? runoff-cands k1) (contains? runoff-cands k2)]
      [true true] (if-let [margin (get-in pg [:adj k1 k2])]
                    (if (zero? margin)
                      (before? k1 k2 tbrc)
                      true)
                    false)
      [false true] false
      [true false] true
      [false false] (score-result-comp tbrc [k1 v1] [k2 v2]))))

(defn star
  "Performs an election using STAR. Takes same arguments as borda-count.
  Returns the same as borda count except the winner of the runoff will be
  the first item in the array-map.

  Note: unlike the raditional definition of STAR voting, no restriction on
  the allowed scores is applied."
  [ballots & {:keys [score-fn tbrc]
              :or {score-fn identity}}]
  (let [tbrc (or tbrc (-> ballots first keys))
        bc (borda-count ballots :score-fn score-fn :tbrc tbrc)
        runoff-cands (->> bc (take 2) (map first))
        ;; use pairwise to perform runoff, invert b/c higher score = better
        pg (->> ballots (map #(select-keys % runoff-cands)) pairwise invert)]
    (into-sorted-array-map
     (partial star-result-comp pg tbrc)
     bc)))
