(ns suffragium.stochastic)

(defn ssv
  "Perform a single stochastic vote election. Returns an ordered list of
  candidate preferences from most to least preferred.

  Each ballot should contain a strict order of candidate preferences."
  [ballots]
  (->> ballots rand-nth (sort-by val) keys))
