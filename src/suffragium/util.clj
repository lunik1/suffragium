(ns suffragium.util
  (:require [medley.core :refer [find-first filter-vals]]))

(defn unwrap-set
  "Returns the member of a cardinality 1 set or nil if set is empty"
  [s]
  (first s))

(defn rvec
  "Reverse a vector"
  [v]
  (vec (reverse v)))

(defn freq=
  "Returns true if the contents of all the supplied colls are the same,
  regardless of order. False otherwise."
  [& colls]
  (apply = (map frequencies colls)))

(defn seq=
  "Returns true if the supplied colls are = after applying seq to each."
  [& colls]
  (apply = (map seq colls)))

(defn before?
  "True if a will seen before b when iterating through coll, else false"
  [a b coll]
  (if (= a b)
    false
    (reduce
     #(cond
        (= %2 a) (reduced true)
        (= %2 b) (reduced false)
        :else false)
     false
     coll)))

(defn index-of
  "Gets the index of x's first appearance in coll without having to worry
  about reflection or portability. Returns nil if x not found."
  [coll x]
  (->> coll (map-indexed vector) (find-first #(-> % peek (= x))) first))

(defn lowkey
  "Get the keys with the lowest associated values in m"
  [m]
  (let [lowval (->> m (apply min-key val) val)]
    (->> m (filter-vals (partial = lowval)) keys)))

(defn into-sorted-array-map
  "Puts the supplied coll into an array map, sorted by comp (defaults to
  compare)."
  ([coll] (into-sorted-array-map compare coll))
  ([comp coll] (apply array-map (mapcat identity (sort comp coll)))))

(defn- score-result-comp
  [tbrc [k1 v1] [k2 v2]]
  (case (compare v1 v2)
    1 true
    -1 false
    0 (before? k1 k2 tbrc)))

