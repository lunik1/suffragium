(defproject suffragium "0.0.3"
  :description "Clojure vote counting library"
  :url "https://gitlab.com/lunik1/suffragium"
  :scm {:name "git" :url "https://gitlab.com/lunik1/suffragium.git"}
  :license {:name "EPL-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :plugins [[lein-cljfmt "0.6.7"]
            [net.totakke/lein-libra "0.1.2"]]
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/math.combinatorics "0.1.6"]
                 [medley "1.3.0"]
                 [aysylu/loom "1.0.2"]
                 [net.cgrand/xforms "0.19.2"]]
  :profiles {:dev {:dependencies [[criterium "0.4.4"]
                                  [net.totakke/libra "0.1.1"]]}}
  :repl-options {:init-ns suffragium.cardinal}
  :jvm-opts ^:replace [])
